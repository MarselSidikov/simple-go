package main

import "fmt"

func PrintInRange(from, to int) {
	for i := from; i <= to; i++ {
		fmt.Printf("%v ", i)
	}
}

func SumInRange(from, to int) (sum, count int) {
	sum = 0
	count = from - to
	for i := from; i <= to; i++ {
		sum += i
	}
	return
}

func main() {
	PrintInRange(10, 15)

	result, count := SumInRange(10, 15)
	fmt.Printf("%v %v ", result, count)
}
