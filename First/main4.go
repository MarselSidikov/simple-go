package main

import (
	"fmt"
	"unsafe"
)

func swap(a, b *int) {
	temp := *a
	*a = *b
	*b = temp
}

// & - взятия адреса от переменной

func main() {
	var a int32 = 5
	//var b int32 = 7

	pointer := &a

	//var pointerOnA *int32

	//pointerOnA = &a

	fmt.Printf("%v ", &a)
	fmt.Printf("%T %v \n", pointer, unsafe.Sizeof(pointer))

	fmt.Printf("%v \n", *pointer)

	x := 10
	y := 15

	swap(&x, &y)

	fmt.Printf("%v %v", x, y)

}
