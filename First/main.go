package main

import "fmt"

func main() {
	// переменные
	var i int
	var j = 5

	// i := 1
	k := 5
	k = 10
	fmt.Printf("%T, %v \n", i, i)
	fmt.Printf("%T, %v \n", j, j)
	fmt.Printf("%T, %v \n", k, k)

	if u := 5; k > 10 {
		fmt.Println(u)
	}

	for i := 0; i < 10; i++ {
		fmt.Printf("%v ", i)
	}

}
