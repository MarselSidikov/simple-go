package main

import "fmt"

// данная функция будет возвращать метод сравнения + логику сравнения

func BubbleSort(compare func(a, b int) int) (sortFunction func(array []int) (sortedArray []int)) {
	sortFunction = func(array []int) (sortedArray []int) {
		for i := len(array) - 1; i >= 0; i-- {
			for j := 0; j < i; j++ {
				if compare(array[j], array[j+1]) >= 0 {
					temp := array[j]
					array[j] = array[j+1]
					array[j+1] = temp
				}
			}
		}
		sortedArray = array
		return
	}

	return
}

func SelectionSort(compare func(a, b int) int) (sortFunction func(array []int) (sortedArray []int)) {

	sortFunction = func(array []int) (sortedArray []int) {
		for i := 0; i < len(array); i++ {
			minIndex := i
			for j := i; j < len(array); j++ {
				if compare(array[j], array[minIndex]) < 0 {
					minIndex = j
				}
			}
			temp := array[i]
			array[i] = array[minIndex]
			array[minIndex] = temp
		}
		sortedArray = array
		return
	}

	return

}

func Sort(
	compare func(a, b int) int,
	sortAlg func(compare func(a, b int) int) func([]int) []int,
	array []int) (sortedArray []int) {
	sortFunction := sortAlg(compare)
	return sortFunction(array)
}

func main() {
	array := []int{5, 1, 6, -10, 11}

	compareAsc := func(a, b int) int {
		return a - b
	}

	compareDesc := func(a, b int) int {
		return b - a
	}

	_ = compareDesc

	sortedArray := Sort(compareAsc, SelectionSort, array)

	fmt.Printf("%v", sortedArray)
}
